#!/usr/bin/env python

import argparse
from libs.MainApplication import MainApplication


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--debug", action='store_true', default=False)
    args = parser.parse_args()
    MainApplication(args.debug)
