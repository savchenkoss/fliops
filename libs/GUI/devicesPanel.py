#! /usr/bin/env python

import tkinter as Tk
from tkinter import messagebox
import time
from threading import Thread
import os


ACTIVE_BUTTON_COLOR = "spring green"
if os.name == 'nt':
    PASSIVE_BUTTON_COLOR = "SystemButtonFace"
else:
    PASSIVE_BUTTON_COLOR = "#d9d9d9"


class FLIWheelGUI(Tk.Frame):
    """
    GUI for FLI filter wheel
    """
    def __init__(self, focus_wheel_panel, device, name):
        self.device = device
        self.filter_names = list(self.device.filters.keys())
        self.window = focus_wheel_panel.window
        self.panel = Tk.Frame(focus_wheel_panel.panel)
        # self.panel.grid(column=0, row=1, padx=5, pady=3)
        self.panel.pack(anchor=Tk.W)
        Tk.Label(self.panel, text=f"{name}:", font=("Times", 12), width=7,
                 justify=Tk.LEFT).grid(column=0, row=0, sticky=Tk.W)
        # Create buttons
        self.buttons = {}
        for idx, filter_name in enumerate(self.filter_names):
            button = Tk.Button(self.panel, text=filter_name, font=("Times", 10, "bold"),
                               command=lambda x=filter_name: self.on_click(x), state='disabled')
            self.buttons[filter_name] = button
            self.buttons[filter_name].grid(column=idx+1, row=0, padx=1)

        # Create a status panel
        self.status_message = Tk.StringVar()
        self.status_message.set("Starting")
        lbl = Tk.Label(self.panel, textvariable=self.status_message,
                       font=("Times", 10), justify=Tk.LEFT, width=8)
        lbl.grid(column=len(self.filter_names)+1, row=0, columnspan=2, sticky=Tk.W, padx=8)

        # Buttons for manual wheel rotation
        # Tk.Button(self.panel, text="LEFT",
        #           command=lambda: self.window.fliwheel.step_motor(-1)).grid(column=0, row=2)
        # Tk.Button(self.panel, text="RIGHT",
        #           command=lambda: self.window.fliwheel.step_motor(1)).grid(column=1, row=2)

        # The FLIWheel GUI is created disabled because it is possible that the
        # device is not ready at this moment. In order not to lock the rest of the
        # GUI creation, we will wait for the device in the background and then
        # unlock the buttons
        Thread(target=self.wait).start()

    def lock(self):
        """
        Lock all buttons
        """
        for button in self.buttons.values():
            button.config(state="disabled")

    def unlock(self):
        """
        Unlock all buttons
        """
        for button in self.buttons.values():
            button.config(state="normal")

    def wait(self):
        """
        Function waits untill the device is ready to use and then
        enables the GUI
        """
        while 1:
            if self.device.is_ready:
                break
            time.sleep(0.05)
        self.buttons[self.device.current_filter_name].config(bg=ACTIVE_BUTTON_COLOR)
        self.status_message.set("Ready")
        self.unlock()

    def on_click(self, filter_name):
        # When the user switches the filter, ze probably wants to start a
        # new sequence from so we can set starting index in the
        # grab panel equal to zero
        self.window.grabPanel.start_idx_value.set("0")
        # Start filter switching in the background
        Thread(target=self.select_filter, args=(filter_name,)).start()

    def select_filter(self, filter_name):
        self.status_message.set("Moving")
        self.lock()
        self.window.grabPanel.on_filter_wheel_move()
        self.buttons[self.device.current_filter_name].config(bg=PASSIVE_BUTTON_COLOR)
        self.device.set_filter(filter_name)
        self.buttons[self.device.current_filter_name].config(bg=ACTIVE_BUTTON_COLOR)
        self.window.grabPanel.on_filter_wheel_stop()
        self.unlock()
        self.status_message.set("Ready")


class FocuserRotatorGUI(Tk.Frame):
    """
    GUI for the fucuser rotator device
    """
    def __init__(self, focus_wheel_panel, device):
        self.window = focus_wheel_panel.window
        self.device = device
        # The main panel
        self.panel = Tk.Frame(focus_wheel_panel.panel)
        self.panel.pack(anchor=Tk.W, pady=5)

        # Focuser GUI
        Tk.Label(self.panel, text="Focus:", font=("Times", 12), width=7,
                 justify=Tk.LEFT).grid(column=0, row=0, sticky=Tk.W)
        # Go left button
        self.go_left_button = Tk.Button(self.panel, text="\u2212", font=("Times", 13, "bold"),
                                        command=self.focuser_go_in, state="disabled")
        self.go_left_button.grid(column=1, row=0, padx=1)
        # Step entry
        self.step_variable = Tk.StringVar()
        self.step_variable.set(500)
        self.move_step_entry = Tk.Entry(self.panel, textvariable=self.step_variable,
                                        width=6, justify=Tk.CENTER, state="disabled")
        self.move_step_entry.grid(column=2, row=0, padx=1)
        # Go right button
        self.go_right_button = Tk.Button(self.panel, text="+", font=("Times", 13, "bold"),
                                         command=self.focuser_go_out, state="disabled")
        self.go_right_button.grid(column=3, row=0, padx=1)
        # Current value
        self.focuser_position_value = Tk.StringVar()
        self.focuser_position_value.set("---")
        self.focuser_position_label = Tk.Label(self.panel, textvariable=self.focuser_position_value,
                                               font=("monospace", 12), justify=Tk.CENTER, state="disabled")
        self.focuser_position_label.grid(column=4, row=0, padx=10)
        # Second row of buttons is in different layout
        self.second_row = Tk.Frame(self.panel)
        self.second_row.grid(column=1, row=1, columnspan=4, sticky=Tk.W)
        # Set temperature point button
        self.set_tcorr_button = Tk.Button(self.second_row, text="Set T-corr", width=9,
                                          command=self.set_tcorr, state="disabled")
        self.set_tcorr_button.grid(column=0, row=0, sticky=Tk.W, padx=0)
        # Go to temperature point button
        self.get_tcorr_button = Tk.Button(self.second_row, text="Get T-corr", width=9,
                                          command=self.get_tcorr, state="disabled")
        self.get_tcorr_button.grid(column=1, row=0, sticky=Tk.W, padx=0)

        # Rotator GUI
        Tk.Label(self.panel, text="Rotator:", font=("Times", 12), width=7,
                 justify=Tk.LEFT).grid(column=0, row=2, sticky=Tk.W, pady=9)
        self.angle_value = Tk.StringVar()
        self.angle_value.set(0)
        self.angle_entry = Tk.Entry(self.panel, textvariable=self.angle_value, width=5,
                                    justify=Tk.CENTER, state="disabled")
        self.angle_entry.grid(column=1, row=2)
        self.angle_entry.bind("<Return>", lambda e: self.toggle_rotation())
        # Rotate button. The behaviour of this button depends on the current state of the
        # rotator. If the rotator is idling now, the buttons starts the rotation. If the
        # rotation is in process the buttons stopps the rotations
        self.toggle_rotation_button = Tk.Button(self.panel, text="Rotate", width=8,
                                                command=self.toggle_rotation, state="disabled")
        self.toggle_rotation_button.grid(column=2, row=2, columnspan=2)
        # Current angle
        self.rotator_position_display_value = Tk.StringVar()
        self.rotator_position_display_value.set(0)
        self.rotator_position_degrees = 0
        self.rotator_position_label = Tk.Label(self.panel, textvariable=self.rotator_position_display_value,
                                               font=("monospace", 12), justify=Tk.CENTER, state="disabled")
        self.rotator_position_label.grid(column=4, row=2)
        self.device_info_updater()
        self.unlock()

    def lock(self, which_device="all"):
        if which_device in ("all", "focuser"):
            self.go_left_button.config(state="disabled")
            self.move_step_entry.config(state="disabled")
            self.go_right_button.config(state="disabled")
            self.focuser_position_label.config(state="disabled")
            self.set_tcorr_button.config(state="disabled")
            self.get_tcorr_button.config(state="disabled")
        if which_device in ("all", "rotator"):
            self.angle_entry.config(state="disabled")
            self.toggle_rotation_button.config(state="disabled")
            self.rotator_position_label.config(state="disabled")

    def unlock(self, which_device="all"):
        if which_device in ("all", "focuser"):
            self.go_left_button.config(state="normal")
            self.move_step_entry.config(state="normal")
            self.go_right_button.config(state="normal")
            self.focuser_position_label.config(state="normal")
            self.set_tcorr_button.config(state="normal")
            self.get_tcorr_button.config(state="normal")
        if which_device in ("all", "rotator"):
            self.angle_entry.config(state="normal")
            self.toggle_rotation_button.config(state="normal")
            self.rotator_position_label.config(state="normal")

    def device_info_updater(self):
        self.focuser_position_value.set(int(self.device.focuser_status["CurrStep"]))
        # Take zero point into account and show the user 'clean' instrumental pa value
        curent_pa = self.device.rotator_status["CurentPA"]/1000
        if curent_pa > 180:
            curent_pa -= 360
        offset = float(self.window.config_params.params["rotator_zero_point"])
        self.rotator_position_degrees = int(round(curent_pa - offset, 0))
        self.rotator_position_display_value.set(str(self.rotator_position_degrees) + "\u00B0")
        self.window.root.after(1000, self.device_info_updater)

    def validate_step(self):
        try:
            shift = int(self.step_variable.get())
        except ValueError:
            messagebox.showwarning(parent=self.panel, title="Warning",
                                   message="Wrong focus step value!")
            return False
        if shift <= 0:
            messagebox.showwarning(parent=self.panel, title="Warning",
                                   message="Focus step value must be positive!")
            return False
        return True

    def focuser_go_in(self):
        if self.validate_step() is False:
            return
        current_position = int(self.device.focuser_status["CurrStep"])
        shift = int(self.step_variable.get())
        new_position = current_position - shift
        self.device.focuser_move_absolute_position(new_position)

    def focuser_go_out(self):
        if self.validate_step() is False:
            return
        current_position = int(self.device.focuser_status["CurrStep"])
        shift = int(self.step_variable.get())
        new_position = current_position + shift
        self.device.focuser_move_absolute_position(new_position)

    def set_tcorr(self):
        current_temperature = float(self.device.focuser_status["CurrTemp"])
        current_position = int(self.device.focuser_status["CurrStep"])
        self.window.tcorr_wrapper.show_add_point_dialogue(current_temperature, current_position)

    def get_tcorr(self):
        current_temperature = float(self.device.focuser_status["CurrTemp"])
        self.window.tcorr_wrapper.show_get_correction_dialogue(current_temperature)

    def toggle_rotation(self):
        if self.device.rotator_status["IsMoving"] == 0:
            # Rotator is not moving. Run movement starting procedure
            try:
                posang = int(self.angle_value.get())
            except ValueError:
                messagebox.showwarning(parent=self.panel, title="Warning",
                                       message="Wrong PosAng value!")
                return False
            if not (-90 < posang < 90):
                messagebox.showwarning(parent=self.panel, title="Warning",
                                       message="Position angle value must be between -90 and 90!")
                return

            new_posang = 1000*(int(self.angle_value.get()) +
                               float(self.window.config_params.params["rotator_zero_point"]))
            if abs(new_posang - self.device.rotator_status["CurentPA"]) < 100:
                return
            self.device.rotator_move_absolute_position(new_posang)
            self.toggle_rotation_button.config(text="Stop")

            # Wait in a background untill the rotation is finished to change the button name back
            def f():
                time.sleep(1)
                while self.device.rotator_status["IsMoving"] == 1:
                    time.sleep(0.1)
                self.toggle_rotation_button.config(text="Rotate")
            Thread(target=f).start()

        else:
            # Rotator is already moving. Halt the movement
            self.device.rotator_halt()


class DevicesPanel(Tk.Frame):
    """
    A Panel to control filter wheels, focuser and rotator
    """
    def __init__(self, window):
        self.window = window
        self.panel = Tk.LabelFrame(self.window.root, text="Filters and focus control")
        self.panel.grid(column=1, row=0, rowspan=3, sticky=Tk.N+Tk.W+Tk.E, padx=5)

        # FLI wheel 1 controls
        self.filterwheel_photo_controls = FLIWheelGUI(self, self.window.filterwheel_photo, name="FW-1")

        # FLI wheel 2 controls
        self.filterwheel_polar_controls = FLIWheelGUI(self, self.window.filterwheel_polar, name="FW-2")

        # Focuser-rotator interface
        self.focuser_rotator_controls = FocuserRotatorGUI(self, self.window.focuser_rotator)

    def lock(self, which_device="all"):
        if which_device in ("all", "wheel_photo"):
            self.filterwheel_photo_controls.lock()
        if which_device in ("all", "wheel_polar"):
            self.filterwheel_polar_controls.lock()
        if which_device in ("all", "focuser", "rotator"):
            self.focuser_rotator_controls.lock(which_device)

    def unlock(self, which_device="all"):
        if which_device in ("all", "wheel_photo"):
            self.filterwheel_photo_controls.unlock()
        if which_device in ("all", "wheel_polar"):
            self.filterwheel_polar_controls.unlock()
        if which_device in ("all", "focuser", "rotator"):
            self.focuser_rotator_controls.unlock(which_device)
