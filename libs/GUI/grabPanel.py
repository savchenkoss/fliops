#! /usr/bin/env python

import tkinter as Tk
from tkinter import ttk
from tkinter import messagebox
from functools import partial
from pathlib import Path
from os import path
import time
from threading import Thread
from astropy.io import fits

from .. import honk


class GrabPanel(Tk.Frame):
    def __init__(self, window):
        self.window = window
        self.grabbing_state = "Idle"  # Can be Idle/Grabbing/Paused
        self.wheels_waiting = 0
        self.panel = Tk.LabelFrame(self.window.root, text="Grab control")
        self.panel.grid(column=1, row=3, sticky=Tk.W+Tk.E+Tk.N, padx=5, pady=1)
        self.ttk_style = ttk.Style()
        self.ttk_style.configure("blue.Horizontal.TProgressbar",
                                 foreground='gray', background='blue')
        self.ttk_style.configure("gray.Horizontal.TProgressbar",
                                 foreground='gray', background='gray')
        self.progress_bar_styles = {"blue": "blue.Horizontal.TProgressbar",
                                    "gray": "gray.Horizontal.TProgressbar"}
        self.next_exposure_parameters = None
        self.script_parameters = None

        # Load the list of known objects
        self.known_objects = set()
        path_to_file = (Path(__file__).parent / "../../data/exposures.dat").resolve()
        for line in open(path_to_file):
            self.known_objects.add(line.split()[0])

        # Object entry
        Tk.Label(self.panel, text="Object name:").grid(column=0, row=0, padx=10, sticky=Tk.W)
        self.object_name_value = Tk.StringVar()
        self.object_name_entry = Tk.Entry(self.panel, textvariable=self.object_name_value, width=13)
        self.object_name_entry.grid(column=1, row=0, sticky=Tk.W, columnspan=2, padx=10)
        self.object_name_entry.bind("<Return>", self.start_sequence_grabbing)
        self.object_name_mark_value = Tk.StringVar()
        self.object_name_mark_label = Tk.Label(self.panel, textvariable=self.object_name_mark_value,
                                               fg="green", height=1, font=(None, 10))
        self.object_name_mark_label.grid(column=2, row=0, padx=20)
        self.object_name_value.trace_add("write", self.object_name_changed)

        # Exposure duration entry
        Tk.Label(self.panel, text="Exposure (sec):").grid(column=0, row=1, padx=10, sticky=Tk.W)
        self.exposure_value = Tk.StringVar()
        self.exposure_entry = Tk.Entry(self.panel, textvariable=self.exposure_value, width=5)
        self.exposure_entry.grid(column=1, row=1, sticky=Tk.W, padx=10)
        self.exposure_entry.bind("<Return>", self.start_sequence_grabbing)

        # Number of exposures entry
        Tk.Label(self.panel, text="Count:").grid(column=0, row=2, padx=10, sticky=Tk.W)
        self.count_value = Tk.StringVar()
        self.count_entry = Tk.Entry(self.panel, textvariable=self.count_value, width=5)
        self.count_entry.grid(column=1, row=2, sticky=Tk.W, padx=10)
        self.count_entry.bind("<Return>", self.start_sequence_grabbing)

        # Starting index entry
        Tk.Label(self.panel, text="Start index:").grid(column=0, row=3, padx=10, sticky=Tk.W)
        self.start_idx_value = Tk.StringVar()
        self.start_idx_value.set(0)
        self.start_idx_entry = Tk.Entry(self.panel, textvariable=self.start_idx_value, width=5)
        self.start_idx_entry.grid(column=1, row=3, sticky=Tk.W, padx=10)
        self.start_idx_entry.bind("<Return>", self.start_sequence_grabbing)

        # Grab sole exposure button
        self.grab_one_button = Tk.Button(self.panel, text="Single frame", width=13,
                                         command=self.start_one_frame_grabbing)
        self.grab_one_button.grid(column=0, row=4)
        self.grab_one_button.bind("<Return>", self.start_one_frame_grabbing)

        # Sequence button: used for starging and aboring sequence grabbing
        self.sequence_button = Tk.Button(self.panel, text="Grab sequence", width=13,
                                         command=self.sequence_button_pressed)
        self.sequence_button.grid(column=0, row=5)
        self.sequence_button.bind("<Return>", self.sequence_button_pressed)

        # Script button: used for starting scripted grab, pausing and resuming sequences
        self.script_button = Tk.Button(self.panel, text="Grab script", width=13, command=self.script_button_pressed)
        self.script_button.grid(column=0, row=6)

        # Exposure count label
        self.desired_exposures_value = Tk.StringVar()
        self.current_expusure_number_value = Tk.StringVar()
        self.desired_exposures_value.set("-")
        self.current_expusure_number_value.set("-")
        self.exposure_count_panel = Tk.Frame(self.panel)
        self.exposure_count_panel.grid(column=1, row=4, columnspan=2)
        Tk.Label(self.exposure_count_panel, textvariable=self.current_expusure_number_value,
                 anchor=Tk.E).grid(column=0, row=0)
        Tk.Label(self.exposure_count_panel, text="/", anchor=Tk.CENTER).grid(column=1, row=0)
        Tk.Label(self.exposure_count_panel, textvariable=self.desired_exposures_value,
                 anchor=Tk.W).grid(column=2, row=0)

        # Exposure progress bar
        self.progress_bar_value = Tk.DoubleVar()
        self.progress_bar_value.set(0.0)
        self.progress_bar = ttk.Progressbar(self.panel, mode='determinate', maximum=1.0,
                                            length=160, variable=self.progress_bar_value)
        self.progress_bar.grid(column=1, row=5, columnspan=2, padx=10)

        # Time left label
        self.time_left_value = Tk.StringVar()
        Tk.Label(self.panel, textvariable=self.time_left_value,
                 anchor=Tk.CENTER).grid(column=1, row=6, columnspan=2)

    def object_name_changed(self, var, index, mode):
        """
        If the name entered by the user is known, place an Ok sign next to the name entry
        """
        entered_name = self.object_name_value.get().lower()
        for obj_name in self.known_objects:
            if entered_name.startswith(obj_name):
                # Name exists
                self.object_name_mark_value.set("\u2611")
                return
        else:
            self.object_name_mark_value.set("")

    def run_progress_bar_in_background(self, time_interval, color="blue"):
        """
        Progress bar will be updated automatically (without a reference to an
        actual procees that progress it displays).
        time_interval [seconds]: how much time does it take to fill the
                                      entire progress bar
        """
        self.kill_progress_bar = True  # Kill all existing pbars just in case
        time.sleep(0.21)
        self.kill_progress_bar = False
        self.progress_bar.configure(style=self.progress_bar_styles[color])

        def progress_cycle():
            t_start = time.time()
            self.progress_bar_value.set(0.0)
            while 1:
                delta = time.time() - t_start
                if (delta > time_interval) or (self.kill_progress_bar is True):
                    self.progress_bar_value.set(0.0)
                    return
                self.progress_bar_value.set(delta/time_interval)
                time.sleep(0.2)
        Thread(target=progress_cycle).start()

    def run_timer_in_background(self, time_interval):
        self.kill_timer = False

        def timer_cycle():
            t_start = time.time()
            while 1:
                t_passed = time.time() - t_start
                t_left = time_interval - t_passed
                self.time_left_value.set("%i seconds left" % int(round(t_left)))
                if (t_left <= 0) or (self.kill_timer is True):
                    self.time_left_value.set("")
                    return
                time.sleep(1.0)
        Thread(target=timer_cycle).start()

    def cancel_programs(self):
        """
        Remove all 'next exposures' and scrits
        """
        # Reset next exposure parameters
        if self.next_exposure_parameters is not None:
            self.next_exposure_parameters = None
            self.window.statusPanel.set_next_exposure_info(None)
        # Reset script if any in action
        if self.script_parameters:
            self.script_parameters = []
            self.window.statusPanel.set_next_exposure_info(None)

    def abort(self):
        """
        Aborts current exposition
        """
        # Stop progress bar and timer
        self.kill_timer = True
        self.kill_progress_bar = True
        self.kill_sleep = True
        # Stop exposure
        self.window.flicamera.cancel_exposure()
        # Check if everything has stopped
        time.sleep(0.21)
        if (self.progress_bar_value.get() > 0):
            self.window.root.after(100, self.abort)

    def sleep(self, seconds):
        """
        Sleep for given number of seconds
        """
        t_start = time.time()
        self.kill_sleep = False
        while (time.time()-t_start < seconds):
            time.sleep(0.1)
            if self.kill_sleep is True:
                return 1  # sleep was aborted
        return 0  # Sleep ended due to timeout

    def on_filter_wheel_move(self):
        """
        Lock the buttons that are used to start exposure while the filter is changing in
        the Idle mode
        """
        self.wheels_waiting += 1
        if self.grabbing_state == "Idle":
            self.grab_one_button.config(state='disabled')
            self.sequence_button.config(state='disabled')
            self.script_button.config(state='disabled')

    def on_filter_wheel_stop(self):
        """
        Unlock the buttons that are used to start exposure
        """
        self.wheels_waiting -= 1
        if (self.grabbing_state == "Idle") and self.wheels_waiting == 0:
            self.grab_one_button.config(state='normal')
            self.sequence_button.config(state='normal')
            self.script_button.config(state='normal')

    def set_grabbing_mode(self, light_sequence=False):
        """
        Set the panel into the grabbing mode (lock all elements that are
        not neccessary for grubbing as locked, etc.)
        """
        self.object_name_entry.config(state='disabled')
        self.exposure_entry.config(state='disabled')
        self.count_entry.config(state='disabled')
        self.start_idx_entry.config(state='disabled')
        self.grab_one_button.config(state='disabled')
        self.sequence_button.config(text="Abort")
        if light_sequence is True:
            self.script_button.config(text="Pause")
            self.script_button.config(state='normal')
        else:
            self.script_button.config(text="Grab script")
            self.script_button.config(state='disabled')
        self.grabbing_state = "Grabbing"

    def set_idle_mode(self):
        """
        Set the panel into the idle (ready to grab) mode
        """
        self.object_name_entry.config(state='normal')
        self.exposure_entry.config(state='normal')
        self.count_entry.config(state='normal')
        self.start_idx_entry.config(state='normal')
        self.grab_one_button.config(state='normal')
        self.sequence_button.config(text="Grab sequence")
        self.script_button.config(text="Grab script")
        self.script_button.config(state="normal")
        self.grabbing_state = "Idle"

    def set_paused_mode(self):
        """
        Set the panel into paused grabbing mode
        """
        self.script_button.config(text="Resume")
        self.grabbing_state = "Paused"
        self.window.devicesPanel.unlock("focuser")

    def validate_sole(self):
        """
        Validates only the exposure entry, that is needed for the sole exposure
        """
        try:
            exposure = float(self.exposure_value.get())
        except ValueError:
            messagebox.showwarning(parent=self.window.root, title="Warning",
                                   message="Wrong exposure value!")
            return False
        if exposure <= 0:
            messagebox.showwarning(parent=self.window.root, title="Warning",
                                   message="Exposure length must be positive!")
            return False
        return True

    def start_one_frame_grabbing(self):
        validate_result = self.validate_sole()
        if validate_result is False:
            return
        photo_filter = self.window.filterwheel_photo.current_filter_name
        polar_filter = self.window.filterwheel_polar.current_filter_name
        if polar_filter != "Empty":
            filter_string = polar_filter + photo_filter
        else:
            filter_string = photo_filter
        self.filter_string = filter_string.lower()

        Thread(target=self.grab_one_frame).start()

    def grab_one_frame(self):
        exposure = float(self.exposure_value.get())
        # Lock panels
        Thread(target=self.grab_lock).start()
        self.window.grabPanel.run_progress_bar_in_background(exposure)
        self.window.grabPanel.run_timer_in_background(exposure)
        light_image = self.window.flicamera.grab_light(1000 * exposure)  # exposure is in milliseconds
        if light_image is not None:
            # Exposure was not stopped
            # Show image
            self.window.imagPanel.image_queue.put((light_image, "Single frame"))
            header = self.make_light_header(0)
            self.window.flicamera.fill_header(header)
            self.window.funcPanel.last_data_and_header = (light_image, header)
        # Unlock panels
        Thread(target=self.grab_unlock).start()

    def validate_sequence(self):
        if len(self.object_name_value.get()) == 0:
            messagebox.showwarning(parent=self.window.root, title="Warning",
                                   message="Empty object name!")
            return False
        try:
            exposure = float(self.exposure_value.get())
        except ValueError:
            messagebox.showwarning(parent=self.window.root, title="Warning",
                                   message="Wrong exposure value!")
            return False
        if exposure <= 0:
            messagebox.showwarning(parent=self.window.root, title="Warning",
                                   message="Exposure must be positive!")
            return False
        try:
            int(self.start_idx_value.get())
        except ValueError:
            messagebox.showwarning(parent=self.window.root, title="Warning",
                                   message="Wrong start idx value!")
            return False
        try:
            count = int(self.count_value.get())
        except ValueError:
            messagebox.showwarning(parent=self.window.root, title="Warning",
                                   message="Wrong count value!")
            return False
        if count < 0:
            messagebox.showwarning(parent=self.window.root, title="Warning",
                                   message="Count must be positive!")
            return False
        return True

    def check_file_existance(self):
        """
        Checks if there are already existing files with the same names, and what to do with them
        if the answer is positive. Possible options: abort grabbing ('Cancel') append files to the
        end ('Append') and overwrite existing files ('overwrite'). If we want to overwrite files,
        we also need to specify the index of the last existing file.
        """
        count = int(self.count_value.get())
        start_idx = int(self.start_idx_value.get())
        object_name = self.object_name_value.get().lower()
        for idx in range(start_idx, count + start_idx):
            out_name = path.join(self.window.path_to_save_files, "%s%sS%i.FIT" % (object_name, self.filter_string, idx))
            if path.exists(out_name):
                answer = messagebox.askokcancel("Files exist!", "Overwrite existing files?")
                return answer
        return True

    def find_max_file_idx(self):
        """
        Finds the index of the last written file for the current
        object and the combination of the filters.
        """
        objname_and_filter = self.object_name_value.get().lower() + self.filter_string
        # Find existing files
        existing_files = Path(self.window.path_to_save_files).glob(f"{objname_and_filter}S*.FIT")
        # extract indices from the file names
        existing_indices = [int(name.stem.split("S")[-1]) for name in existing_files]
        if len(existing_indices) != 0:
            return max(existing_indices)
        return 0

    def sequence_button_pressed(self):
        if self.grabbing_state == "Idle":
            self.start_sequence_grabbing()
        elif self.grabbing_state == "Grabbing":
            self.cancel_programs()
            self.abort()
        elif self.grabbing_state == "Paused":
            self.set_idle_mode()

    def start_sequence_grabbing(self, overwrite=False):
        # Do not initiate a grabbing while filters are still moving
        if self.window.devicesPanel.filterwheel_photo_controls.status_message.get() == "Moving":
            return
        if self.window.devicesPanel.filterwheel_polar_controls.status_message.get() == "Moving":
            return
        # Find a filename substring that shows the filter combination
        photo_filter = self.window.filterwheel_photo.current_filter_name
        polar_filter = self.window.filterwheel_polar.current_filter_name
        if polar_filter != "Empty":
            filter_string = polar_filter + photo_filter
        else:
            filter_string = photo_filter
        self.filter_string = filter_string.lower()

        validate_result = self.validate_sequence()
        if validate_result is False:
            return

        if (int(self.start_idx_value.get()) >= 0) and (overwrite is False):
            # We only want to check for file existance if the starting index is
            # non-negative. If it is negative, the programm will append the new
            # files in the end anyway, so the existing files are safe
            if self.check_file_existance() is False:
                # Files exist and user hit cancel
                return
        Thread(target=self.grab_sequence).start()

    def grab_lock(self, light_sequence=False):
        """
        Lock function, filterwheel and grab panels so that the user cannot mess with
        anithing while the light grabbing is in progress
        """
        self.window.grabPanel.set_grabbing_mode(light_sequence=light_sequence)
        self.window.funcPanel.lock()
        self.window.devicesPanel.lock()

    def grab_unlock(self):
        self.window.grabPanel.set_idle_mode()
        self.window.funcPanel.unlock()
        self.window.devicesPanel.unlock()

    def script_button_pressed(self):
        if self.grabbing_state == "Idle":
            self.run_script()
        elif self.grabbing_state == "Grabbing":
            self.pause_grabbing()
        elif self.grabbing_state == "Paused":
            self.resume_grabbing()

    def grab_sequence(self):
        exposure = float(self.exposure_value.get())
        count = int(self.count_value.get())
        object_name = self.object_name_value.get().lower()
        start_idx = int(self.start_idx_value.get())
        if start_idx < 0:
            start_idx = self.find_max_file_idx() + 1
        if count > 0:
            stop_idx = count + start_idx
            self.window.grabPanel.desired_exposures_value.set(count)

        else:
            stop_idx = 99999
            self.window.grabPanel.desired_exposures_value.set("\u221e")

        # Lock
        Thread(target=self.grab_lock, args=(True,)).start()

        # Grab light images
        idx = start_idx
        while idx < stop_idx:
            if self.grabbing_state == "Paused":
                while self.grabbing_state == "Paused":
                    # If the sequence is on pause, wait untill it released
                    time.sleep(0.1)
                if self.grabbing_state == "Idle":
                    # The sequence was aborted while on pause
                    break
            self.window.grabPanel.current_expusure_number_value.set(idx + 1 - start_idx)
            self.window.grabPanel.run_progress_bar_in_background(exposure)
            self.window.grabPanel.run_timer_in_background(exposure)
            # Shot frame and retrieve it from camera
            if (self.window.exposure_delay > 0) and (idx < (count + start_idx - 1)):
                # Tell the camera to emit zeromq signal when the shutter was closed, so the
                # dithering will be performed
                # Exposure is in seconds
                light_image = self.window.flicamera.grab_light(1000 * exposure, emit_signal=True)
            else:
                # Do not emit signal, no dithering.
                light_image = self.window.flicamera.grab_light(1000 * exposure, emit_signal=False)
            if (light_image is None) and (self.grabbing_state == "Grabbing"):
                # Exposure was aborted
                break
            if (light_image is None) and (self.grabbing_state == "Paused"):
                # Sequence was set on pause
                continue
            # Fill header
            header = self.make_light_header(idx)
            self.window.flicamera.fill_header(header)
            # Save file
            out_name = path.join(self.window.path_to_save_files, "%s%sS%i.FIT" % (object_name, self.filter_string, idx))
            out_hdu = fits.PrimaryHDU(data=light_image, header=header)
            out_hdu.header["BSCALE"] = 1
            out_hdu.header["BZERO"] = 0
            out_hdu.writeto(out_name, overwrite=True)
            # Show image
            self.window.imagPanel.image_queue.put((light_image, path.basename(out_name)))
            # Store the image so it can be manually saved
            self.window.funcPanel.last_data_and_header = (light_image, header)
            # Wait delay before taking new exposure
            if (self.window.exposure_delay > 0) and (idx < (count + start_idx - 1)) and \
               self.window.signal_sender.is_dithering_on():
                self.run_progress_bar_in_background(self.window.exposure_delay, color="gray")
                self.run_timer_in_background(self.window.exposure_delay)
                if (self.sleep(self.window.exposure_delay) == 1) and (self.grabbing_state == "Grabbing"):
                    # Exposure was aborted during the delay phase
                    break
                if (self.sleep(self.window.exposure_delay) == 1) and (self.grabbing_state == "Pauserd"):
                    # Exposure was paused during the delay phase
                    continue
            if self.window.alarm_active and (idx == (count + start_idx - 2)):
                # Second to last exposure just finished
                honk.honk_high()
            elif self.window.alarm_active and (idx == (count + start_idx - 1)):
                # Last exposure just finished
                honk.honk_low()
            idx += 1

        # Remove the progress information from the grab panel
        self.window.grabPanel.current_expusure_number_value.set("-")
        self.window.grabPanel.desired_exposures_value.set("-")
        if self.next_exposure_parameters is not None:
            # The user provided next exposure parameters during the exposure
            self.auto_exposure()
        else:
            # Unlock grab panel if there is no scripted exposures
            self.grab_unlock()

    def setup_next_exposure(self, next_exposure_parameters):
        self.next_exposure_parameters = next_exposure_parameters

    def auto_exposure(self):
        """
        Sets preset exposure parameters and stars next exposure
        """
        self.window.devicesPanel.filterwheel_photo_controls.on_click(self.next_exposure_parameters['photo'])
        self.window.devicesPanel.filterwheel_polar_controls.on_click(self.next_exposure_parameters['polar'])
        self.exposure_value.set(str(self.next_exposure_parameters['exposure']))
        self.count_value.set(str(self.next_exposure_parameters['count']))
        self.start_idx_value.set(str(self.next_exposure_parameters['index']))
        if self.script_parameters:
            params = self.script_parameters.pop(0)
            self.next_exposure_parameters['photo'] = params[0]
            self.next_exposure_parameters['polar'] = params[1]
            self.next_exposure_parameters['exposure'] = params[2]
            self.next_exposure_parameters['count'] = params[3]
            self.next_exposure_parameters['index'] = params[4]
            self.window.statusPanel.set_next_exposure_info(self.next_exposure_parameters)
        else:
            self.next_exposure_parameters = None
            self.window.statusPanel.set_next_exposure_info(None)

        def wait_filters():
            while 1:
                # Wait for photometric filter wheel to finish it rotation
                if self.window.devicesPanel.filterwheel_photo_controls.status_message.get() == "Moving":
                    time.sleep(0.01)
                    continue
                else:
                    break
            while 1:
                # Wait for polarimetric filter wheel to finish it rotation
                if self.window.devicesPanel.filterwheel_polar_controls.status_message.get() == "Moving":
                    time.sleep(0.01)
                    continue
                else:
                    break
            # When both filters are ready we can start new exposure
            self.start_sequence_grabbing()
        Thread(target=wait_filters).start()

    def run_script(self):
        if len(self.object_name_value.get()) == 0:
            messagebox.showwarning(parent=self.window.root, title="Warning",
                                   message="Empty object name!")
            return False
        ScriptWindow(self.window, self.object_name_value.get().lower())

    def pause_grabbing(self):
        self.set_paused_mode()
        self.abort()

    def resume_grabbing(self):
        self.window.devicesPanel.lock("focuser")
        self.set_grabbing_mode(True)

    def grab_test_frame(self):
        """
        Grabs a single frame in R-band without a polarization mode with 3-sec exposure. This frame
        will be used by a SkyPointer program to adjust telescope pointing
        """
        # Set r-filter
        self.window.devicesPanel.filterwheel_photo_controls.on_click("R")
        self.window.devicesPanel.filterwheel_polar_controls.on_click("Empty")
        self.exposure_value.set('3')
        self.count_value.set('1')
        self.start_idx_value.set('0')
        self.object_name_value.set('pointing')

        def wait_filters():
            while 1:
                # Wait for photometric filter wheel to finish it rotation
                if self.window.devicesPanel.filterwheel_photo_controls.status_message.get() == "Moving":
                    time.sleep(0.01)
                    continue
                else:
                    break
            while 1:
                # Wait for polarimetric filter wheel to finish it rotation
                if self.window.devicesPanel.filterwheel_polar_controls.status_message.get() == "Moving":
                    time.sleep(0.01)
                    continue
                else:
                    break
            # When both filters are ready we can start new exposure
            self.start_sequence_grabbing(overwrite=True)
        Thread(target=wait_filters).start()

    def make_light_header(self, idx):
        """
        Create a header for a light shot
        """
        time_string = time.strftime("%Y-%m-%dT%H:%M:%S", time.gmtime())
        header = fits.Header()
        header["BSCALE"] = 1
        header["BZERO"] = 0
        header["IMAGETYP"] = 'Light Frame'
        header["TOUTSIDE"] = str(self.window.focuser_rotator.focuser_status["CurrTemp"]) + " C"
        header["ROTATION"] = str(self.window.devicesPanel.focuser_rotator_controls.rotator_position_degrees)+" deg"
        header["DITHER"] = self.window.signal_sender.is_dithering_on()
        header["FILTER"] = "+".join(self.filter_string)
        header["TELESCOP"] = 'LX200 16"'
        header["DATE"] = time_string
        header["DATE-OBS"] = time_string
        header["FRIDX"] = idx
        ra, dec, target = self.window.signal_sender.get_telescope_pointing()
        if ra is not None:
            header["RA"] = float(ra)  # In degrees
        if dec is not None:
            header["DEC"] = float(dec)
        if target is not None:
            header["OBJECT"] = target
        return header


class ScriptWindow(Tk.Frame):
    """
    The window to set up a script exposure
    """
    def __init__(self, window, objname):
        self.window = window
        self.objname = objname
        self.script_file_name = Path(__file__).parent / "../../data/exposures.dat"
        self.top = Tk.Toplevel(self.window.root)
        self.top.title("Set up a script exposure")
        self.top.geometry('+%i+%i' % (self.window.root.winfo_x()+80,
                                      self.window.root.winfo_y()+80))
        self.top.attributes('-topmost', 'true')
        self.top.resizable(0, 0)
        self.window.popup_exists = True
        self.true_object_name = None

        # Read exposures from a data file
        scripts = []
        script_lines = []
        for line in open(self.script_file_name):
            if line.startswith("#"):
                continue
            params = line.split()
            if objname.startswith(params[0].lower()):
                self.true_object_name = params[0].lower()
                exposures = []
                for value in params[1:]:
                    filt = value.split(":")[0].upper()
                    exposure = value.split(":")[1].split("|")[0]
                    number = value.split(":")[1].split("|")[1]
                    exposures.append((filt, exposure, number))
                if exposures not in scripts:
                    scripts.append(exposures)
                    script_lines.append(line)
        self.scripts = scripts[-3:]
        self.script_lines = script_lines[-3:]

        # Show exposures choises in the window
        self.delete_buttons = []
        self.script_radio_buttons = []
        self.script_choise_value = Tk.IntVar()
        script_idx = 0
        for script_idx, script in enumerate(self.scripts):
            text = ""
            for exposure in script:
                text += f"{exposure[0]}: {exposure[1]}/{exposure[2]}   "
            rb = Tk.Radiobutton(self.top, text=text, variable=self.script_choise_value, font=8, value=script_idx)
            rb.grid(column=0, row=script_idx, sticky=Tk.W, columnspan=12)
            self.script_radio_buttons.append(rb)
            delete = Tk.Button(self.top, text="Delete", command=partial(self.delete_script, script_idx=script_idx))
            delete.grid(column=11, row=script_idx)
            self.delete_buttons.append(delete)
        self.script_choise_value.set(0)
        self.script_choise_value.trace_add("write", self.radiobutton_toggled)

        # Show entries for manual scripting
        self.available_filters = ["B", "V", "R", "I"]
        self.filters_choises = []
        for idx, filt in enumerate(self.available_filters):
            self.filters_choises.append(Tk.StringVar())
            self.filters_choises[-1].set("-")
            self.filters_choises[-1].trace_add("write", self.filter_choise_toggled)

        # First, add polarisation info
        panel = Tk.Frame(self.top)
        panel.grid(column=0, row=script_idx+1, sticky=Tk.E, pady=10)
        Tk.Label(panel, text="Polar", font=8).grid(row=0, column=0)
        self.polar_filter = Tk.StringVar()
        self.polar_exposure_entry_value = Tk.StringVar()
        self.polar_number_entry_value = Tk.StringVar()
        options = ["None"]
        options.extend(self.available_filters)
        self.polar_menu = Tk.OptionMenu(panel, self.polar_filter, *options)
        self.polar_menu.config(width=4)
        self.polar_menu.grid(row=1, column=0)
        panel = Tk.Frame(self.top)
        panel.grid(column=1, row=script_idx+1, sticky=Tk.W, pady=10)
        self.polar_exposure_entry = Tk.Entry(panel, width=4, textvariable=self.polar_exposure_entry_value)
        self.polar_exposure_entry.grid(column=0, row=0, sticky=Tk.W)
        self.polar_number_entry = Tk.Entry(panel, width=4, textvariable=self.polar_number_entry_value)
        self.polar_number_entry.grid(column=0, row=1, sticky=Tk.W)
        self.polar_filter.trace_add("write", self.polar_filter_toggled)
        self.polar_filter.set("None")

        # Add the rest of the filters
        self.exposure_entries = []
        self.number_entries = []
        self.exposure_entry_values = []
        self.number_entry_values = []
        for filt_idx in range(len(self.filters_choises)):
            self.exposure_entry_values.append(Tk.StringVar())
            self.number_entry_values.append(Tk.StringVar())
            options = self.available_filters + ["-"]
            menu = Tk.OptionMenu(self.top, self.filters_choises[filt_idx], *options)
            menu.config(width=1)
            menu.grid(column=2*(filt_idx+1), row=script_idx+1, padx=5, sticky=Tk.E)
            panel = Tk.Frame(self.top)
            panel.grid(column=2*(filt_idx+1)+1, row=script_idx+1, sticky=Tk.W, pady=10)
            entry = Tk.Entry(panel, width=4, textvariable=self.exposure_entry_values[filt_idx], state="disabled")
            entry.grid(column=0, row=0, sticky=Tk.W)
            self.exposure_entries.append(entry)
            entry = Tk.Entry(panel, width=4, textvariable=self.number_entry_values[filt_idx], state="disabled")
            entry.grid(column=0, row=1, sticky=Tk.W)
            self.number_entries.append(entry)
        Tk.Label(self.top, text="  ", font=8).grid(column=2*(filt_idx+1)+2, row=script_idx+1)

        # Show the buttons
        buttons_panel = Tk.Frame(self.top)
        buttons_panel.grid(column=0, row=script_idx+2, columnspan=12, sticky=Tk.W, padx=15, pady=10)
        # A button for starting the script
        start_button = Tk.Button(buttons_panel, text="Start", command=self.start_button_pressed)
        start_button.grid(column=0, row=0, sticky=Tk.W)
        # A button for starting the script along with saving the script into the file
        start_save_button = Tk.Button(buttons_panel, text="Save & Start", command=self.save_and_start_button_pressed)
        start_save_button.grid(column=1, row=0, sticky=Tk.W)
        # A cancel button
        cancel_button = Tk.Button(buttons_panel, text="Cancel", command=self.onclose)
        cancel_button.grid(column=2, row=0, sticky=Tk.E, padx=50)
        self.top.protocol('WM_DELETE_WINDOW', self.onclose)

    def delete_script(self, script_idx):
        keep = []
        for line in open(self.script_file_name):
            if line.strip() == self.script_lines[script_idx].strip():
                # This line won't be saved
                continue
            keep.append(line)
        fout = open(self.script_file_name, "w")
        fout.writelines(keep)
        fout.close()
        self.script_radio_buttons[script_idx].config(state="disabled")
        self.delete_buttons[script_idx].config(state="disabled")

    def radiobutton_toggled(self, var, index, mode):
        """
        When the radiobutton is toggled, set the selected script parameters into
        the manual entries
        """
        # Remove all old values firs
        for idx in range(len(self.exposure_entry_values)):
            self.exposure_entry_values[idx].set("")
            self.number_entry_values[idx].set("")
            self.filters_choises[idx].set("-")
        self.polar_filter.set("None")
        self.polar_exposure_entry_value.set("")
        self.polar_number_entry_value.set("")
        # Fill in the new values
        script_idx = self.script_choise_value.get()
        filters_filled = 0
        for filt_idx in range(len(self.scripts[script_idx])):
            filt = self.scripts[script_idx][filt_idx][0]
            exposure = self.scripts[script_idx][filt_idx][1]
            number = self.scripts[script_idx][filt_idx][2]
            if filt.startswith("POL"):
                self.polar_filter.set(filt[filt.index("(")+1])
                self.polar_exposure_entry_value.set(exposure)
                self.polar_number_entry_value.set(number)
            else:
                self.exposure_entry_values[filters_filled].set(exposure)
                self.number_entry_values[filters_filled].set(number)
                self.filters_choises[filters_filled].set(filt.upper())
                filters_filled += 1

    def polar_filter_toggled(self, var, index, mode):
        if self.polar_filter.get() == "None":
            self.polar_number_entry.config(state="disabled")
            self.polar_exposure_entry.config(state="disabled")
        else:
            self.polar_number_entry.config(state="normal")
            self.polar_exposure_entry.config(state="normal")

    def filter_choise_toggled(self, var, index, mode):
        """
        Invoked when the option menu related to the filter choise is changed
        """
        entry_idx = [str(v) for v in self.filters_choises].index(var)
        if self.filters_choises[entry_idx].get() == "-":
            self.number_entries[entry_idx].config(state="disabled")
            self.exposure_entries[entry_idx].config(state="disabled")
        else:
            self.number_entries[entry_idx].config(state="normal")
            self.exposure_entries[entry_idx].config(state="normal")

    def start_button_pressed(self):
        if not self.validate():
            return
        # Combine all scripted parameters in one list
        script_to_run = []
        # Add polarisation in script (if specified)
        if self.polar_filter.get() != "None":
            script_to_run.append((self.polar_filter.get(), "X", self.polar_exposure_entry_value.get(),
                                  self.polar_number_entry_value.get(), 0))
            script_to_run.append((self.polar_filter.get(), "Y", self.polar_exposure_entry_value.get(),
                                  self.polar_number_entry_value.get(), 0))
        filters = [c.get() for c in self.filters_choises]
        # Add photometry filters
        for idx, filt in enumerate(filters):
            if filt == "-":
                continue
            exposure = float(self.exposure_entry_values[idx].get())
            number = int(self.number_entry_values[idx].get())
            script_to_run.append((filt, "Empty", exposure, number, 0))
        # Fill the next exposure parameters
        self.window.grabPanel.next_exposure_parameters = {}
        self.window.grabPanel.next_exposure_parameters['photo'] = script_to_run[0][0]
        self.window.grabPanel.next_exposure_parameters['polar'] = script_to_run[0][1]
        self.window.grabPanel.next_exposure_parameters['exposure'] = script_to_run[0][2]
        self.window.grabPanel.next_exposure_parameters['count'] = script_to_run[0][3]
        self.window.grabPanel.next_exposure_parameters['index'] = script_to_run[0][4]
        # Store the rest of the script
        self.window.grabPanel.script_parameters = script_to_run[1:]
        self.window.grabPanel.auto_exposure()
        self.onclose()

    def save_and_start_button_pressed(self):
        if not self.validate():
            return
        answer = messagebox.askokcancel("Save new script?", "Save new script?", parent=self.top)
        if answer is False:
            # The user has decided not to save new script
            return
        # Save new script into the file
        out_string = f"{self.true_object_name}    "
        if self.polar_filter.get() != "None":
            out_string += f"Pol({self.polar_filter.get()}):{self.polar_exposure_entry_value.get()}|"
            out_string += f"{self.polar_number_entry_value.get()} "
        for idx in range(len(self.exposure_entry_values)):
            filt = self.filters_choises[idx].get()
            if filt != "-":
                out_string += f"{filt}:{self.exposure_entry_values[idx].get()}|{self.number_entry_values[idx].get()} "
        all_scripts = open(self.script_file_name).readlines()
        for line in all_scripts:
            if out_string.strip().lower() == line.strip().lower():
                # There is already such script in a file, no need to add another one
                break
        else:
            # No such script in a file, let's add it
            fout = open(self.script_file_name, "a")
            fout.write(out_string+"\n")
            fout.close()
        self.start_button_pressed()

    def validate(self):
        """
        Validate entry values before applying changes
        """
        if (self.polar_filter.get() == "None") and all([f.get() == "-" for f in self.filters_choises]):
            messagebox.showwarning(parent=self.top, title="Warning",
                                   message="Can't run an empty script.")
            return False
        if self.polar_filter.get() != "None":
            try:
                float(self.polar_exposure_entry_value.get())
            except ValueError:
                messagebox.showwarning(parent=self.top, title="Warning",
                                       message="Wrong exposure length value for polarisation")
                return False
            if float(self.polar_exposure_entry_value.get()) <= 0:
                messagebox.showwarning(parent=self.top, title="Warning",
                                       message="Polarisation exposure length should be positive")
                return False
            try:
                int(self.polar_number_entry_value.get())
            except ValueError:
                messagebox.showwarning(parent=self.top, title="Warning",
                                       message="Wrong exposure number value for polarisation")
                return False
            if int(self.polar_number_entry_value.get()) <= 0:
                messagebox.showwarning(parent=self.top, title="Warning",
                                       message="Polarisation exposure number should be positive")
                return False
        filters = [c.get() for c in self.filters_choises if c.get() != "-"]

        if len(filters) != len(set(filters)):
            messagebox.showwarning(parent=self.top, title="Warning",
                                   message="All photometric filters should be unique")
            return False
        for idx in range(len(self.exposure_entry_values)):
            filt = self.filters_choises[idx].get()
            if filt != "-":
                try:
                    float(self.exposure_entry_values[idx].get())
                except ValueError:
                    messagebox.showwarning(parent=self.top, title="Warning",
                                           message=f"Wrong exposure length value for {filt} filter")
                    return False
                if float(self.exposure_entry_values[idx].get()) <= 0:
                    messagebox.showwarning(parent=self.top, title="Warning",
                                           message=f"Exposure length should be positive value [{filt}]")
                    return False
                try:
                    int(self.number_entry_values[idx].get())
                except ValueError:
                    messagebox.showwarning(parent=self.top, title="Warning",
                                           message=f"Wrong exposure number value for {filt} filter")
                    return False
                if int(self.number_entry_values[idx].get()) <= 0:
                    messagebox.showwarning(parent=self.top, title="Warning",
                                           message=f"Exposure number should be positive value [{filt}]")
                    return False
        return True

    def onclose(self):
        self.window.popup_exists = False
        self.top.destroy()
