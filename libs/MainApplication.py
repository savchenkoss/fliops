#! /usr/bin/env python

import tkinter as Tk
from pathlib import Path
from tkinter import messagebox

from .fliwheel import FLIWheel
from .focuser_rotator import GeminiFocuserRotator
from .flicamera import FLICamera
from .errors import DeviceIsNotAvailableError
from .config import ConfigParams
from .temp_corr import TCorrWrapper
from . import signal_sender
from .GUI.funcPanel import FuncPanel
from .GUI.imagPanel import ImagPanel
from .GUI.devicesPanel import DevicesPanel
from .GUI.grabPanel import GrabPanel
from .GUI.statusPanel import StatusPanel
from .GUI.HistogramPanel import HistogramPanel


class MainApplication(Tk.Frame):
    def __init__(self, debug):
        # Program-wide definitions
        if debug is True:
            self.path_to_save_files = "."
        else:
            self.path_to_save_files = "/mnt/TelescopeData"
        self.dark_number = 0
        self.alarm_active = True
        self.alive = True
        self.exposure_delay = 3.5
        self.config_params = ConfigParams()
        # Create a redis signal sender object
        self.signal_sender = signal_sender.Signal()
        # popup_exists variable is True if some popup window exists already
        # to prevent several popups to be invoked simultaneously
        self.popup_exists = False
        self.focus_running = False

        # Create root window
        # Create main window base frame
        self.root = Tk.Tk()
        icon_path = str(Path(__file__).parent / "icon.png")
        photo = Tk.PhotoImage(file=icon_path)
        self.root.wm_iconphoto(False, photo)

        # At first lets create objects for every device we are going to use
        # Connect to the first filter wheel (the one with optical filters)
        filters = {"U": 0, "B": 1, "V": 2, "R": 3, "I": 4}
        try:
            self.filterwheel_photo = FLIWheel(filters=filters, default_filter="U",
                                              name="FLI-010103", bias=0, debug=debug)
        except DeviceIsNotAvailableError as e:
            self.root.withdraw()
            messagebox.showerror(parent=self.root, title="Error", message=str(e))
            exit(1)
        # Connect to the second FLI filter wheel (the one with polarization filters)
        filters = {"X": 5, "Y": 6, "Empty": 7}
        try:
            self.filterwheel_polar = FLIWheel(filters=filters, default_filter="X",
                                              name="FLI-010102", bias=1, debug=debug)
        except DeviceIsNotAvailableError as e:
            self.root.withdraw()
            messagebox.showerror(parent=self.root, title="Error", message=str(e))
            self.filterwheel_photo.shutdown()
            exit(1)

        # Conntect to the focuser-rotator
        try:
            self.focuser_rotator = GeminiFocuserRotator(debug)
        except DeviceIsNotAvailableError as e:
            self.root.withdraw()
            messagebox.showerror(parent=self.root, title="Error", message=str(e))
            self.filterwheel_photo.shutdown()
            self.filterwheel_polar.shutdown()
            exit(1)

        # Connect to FLI camera
        try:
            self.flicamera = FLICamera(self.signal_sender, debug=debug)
        except DeviceIsNotAvailableError as e:
            # Camera is not responding. Let's shut down everything and exit
            self.root.withdraw()
            messagebox.showerror(parent=self.root, title="Error", message=str(e))
            self.filterwheel_photo.shutdown()
            self.filterwheel_polar.shutdown()
            self.focuser_rotator.shutdown()
            exit(1)

        # Configure root window
        self.root.title("FLI Ops")
        self.root.protocol('WM_DELETE_WINDOW', self.shutdown)
        self.root.resizable(0, 0)

        # Load a function buttons panel
        self.funcPanel = FuncPanel(self)
        # Load an images panel
        self.imagPanel = ImagPanel(self)
        # Load a panel for controlling focus and filter wheels
        self.devicesPanel = DevicesPanel(self)
        # Load a panel for image grabbing
        self.grabPanel = GrabPanel(self)
        # Load a histogram panel
        self.histogramPanel = HistogramPanel(self)
        # Load a status panel
        self.statusPanel = StatusPanel(self)

        # Start listening for incomming messages
        # Thread(target=self.listen_test_frame_comand).start()

        # Create a temperature correction wrapper instance
        self.tcorr_wrapper = TCorrWrapper(self)
        self.root.mainloop()

    # def listen_test_frame_comand(self):
    #     while self.alive:
    #         try:
    #             msg = self.test_frame_socket.recv().decode()
    #             print("Got new messange:", msg)
    #         except zmq.error.Again:
    #             time.sleep(0.05)
    #             continue
    #         if msg == "test":
    #             self.grabPanel.grab_test_frame()

    def shutdown(self):
        """
        This function is called when the user hit close window button
        """
        answer = messagebox.askokcancel("Close", "Really close?")
        if answer is True:
            self.alive = False
            # Shutting down devices
            self.devicesPanel.filterwheel_photo_controls.status_message.set("Closing")
            self.devicesPanel.filterwheel_polar_controls.status_message.set("Closing")
            self.root.after(0, self.filterwheel_photo.shutdown)
            self.root.after(0, self.filterwheel_polar.shutdown)
            self.root.after(0, self.focuser_rotator.shutdown)
            # Stopping background processes
            self.imagPanel.image_queue.put(None)
            # Close the main window
            self.root.after(100, self.close)

    def close(self):
        if not any([self.filterwheel_photo.is_alive, self.filterwheel_polar.is_alive, self.focuser_rotator.is_alive]):
            self.signal_sender.close()
            self.root.destroy()
        else:
            self.root.after(100, self.close)
