#! /usr/bin/env python

import time
import os
from serial import SerialException
from .SerialDevice import SerialDevice
from .errors import DeviceIsNotAvailableError


class FakeDevice(object):
    def __init__(self):
        self.psn = 0

    def close(self):
        time.sleep(0.5)

    def send_command(self, command):
        time.sleep(1)
        self.psn += int(command)
        pass

    def readline(self):
        return "position %i" % (self.psn)


class Focuser(object):
    def __init__(self):
        self.position = 0

        # Initialise wheel
        if os.name == 'nt':
            try:
                self.device = SerialDevice("COM4", baudrate=9600)
            except SerialException:
                raise DeviceIsNotAvailableError("Focuser is not available")
        else:
            self.device = FakeDevice()
        self.move(0, "right")
        self.is_alive = True

    def shutdown(self):
        self.device.close()
        self.is_alive = False

    def move(self, step, direction):
        if direction == "left":
            step = -1 * step
        command = "%i\n" % step
        self.device.send_command(command)
        # Read response untill current relative position is returned
        while 1:
            line = self.device.readline()
            if "position" in line:
                break
        # Find current position from the response
        try:
            self.position = int(line.split()[-1])
        except ValueError:
            return

    def reset(self):
        """
        Resets focuser's relative position to zero
        """
        self.device.send_command("reset\n")
        time.sleep(0.25)
        self.device.flush()
        self.move(0, "right")
