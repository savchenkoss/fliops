#! /usr/bin/env python

from threading import Thread
import subprocess
from pathlib import Path


high_name = Path(__file__).parent / "beep_high.mp3"
low_name = Path(__file__).parent / "beep_low.mp3"


def honk_high():
    Thread(target=subprocess.call, args=(f"mplayer {high_name}",),
           kwargs={"shell": True}).start()


def honk_low():
    """
    Produces a sound. Only works on NT
    """
    Thread(target=subprocess.call, args=(f"mplayer {low_name}",),
           kwargs={"shell": True}).start()
