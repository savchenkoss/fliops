#! /usr/bin/env python

import redis
import time
import threading
import queue
from queue import Empty


class Signal(object):
    def __init__(self):
        self.redis = redis.Redis(host='localhost', port=6379, decode_responses=True)
        self.keep_alive = True
        self.messages_queue = queue.Queue()
        threading.Thread(target=self.initiate).start()
        threading.Thread(target=self.ping_repeatedly).start()

    def initiate(self):
        self.redis.delete("fliops_commands")
        with self.messages_queue.mutex:
            self.messages_queue.queue.clear()
        self.emit_signals()

    def send_signal(self, message):
        self.redis.lpush("fliops_commands", message)

    def emit_signals(self):
        while self.keep_alive:
            try:
                # Try to get a next message from the queue
                new_message = self.messages_queue.get(timeout=0.05)
            except Empty:
                # No messages in queue
                continue
            # There is a message in the queue, let's try to send it
            self.send_signal(new_message)

    def ping_repeatedly(self):
        while self.keep_alive:
            time.sleep(1)
            self.redis.setex("fliops_running", time=2, value=1)

    def is_dithering_on(self):
        if self.redis.get("dithering") == "on":
            return True
        return False

    def get_telescope_pointing(self):
        ra = self.redis.get("telescope_ra")  # In degrees
        dec = self.redis.get("telescope_dec")
        target = self.redis.get("telescope_target")
        return ra, dec, target

    def close(self):
        self.keep_alive = False
